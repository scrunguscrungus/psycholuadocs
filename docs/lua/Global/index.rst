Global
======

Script classes that exist in the ``Global`` domain.

.. toctree::
   :maxdepth: 2
   
   GlobalClass