GlobalClass
===========

.. toctree::
   :maxdepth: 2
   :hidden:

.. note:: |WIP|

.. lua:class:: global.GlobalClass

	A global megaclass that does many different things. Created by the engine automatically.

	Accessed through the ``Global`` global variable.

	.. lua:attribute:: levelScript:LevelScript

		Reference of the current level's level script.
