GEN_dartstart
=============

.. toctree::
   :maxdepth: 2
   :hidden:

.. lua:class:: GEN_dartstart: ScriptBase

	This class is used as a spawn point for Raz.

	.. lua:attribute:: bDontSnap: bool

		EditVar.

		If set, then the game will not try to snap the player to the ground when spawning them here.

	.. lua:method:: onBeginLevel(self)

		:virtual:

		:param self: This entity
		:type self: GEN_dartstart

		Sets ``Global.levelScript.dartStart`` to this entity.
