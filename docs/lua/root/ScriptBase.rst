ScriptBase
==========

.. toctree::
   :maxdepth: 2
   :hidden:

.. note:: |WIP|

.. lua:class:: ScriptBase

	The base class for all scripts. Every class should ultimately derive from this at the end of their inheritance chain. 