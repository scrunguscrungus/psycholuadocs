boolean
=================

:orphan:

.. toctree::
   :maxdepth: 2
   :hidden:

.. lua:class:: bool

    0 or 1
