.. Psychonauts Lua documentation master file, created by
   sphinx-quickstart on Wed Feb 23 02:16:04 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Psychonauts Lua API documentation
==================================

.. IMPORTANT::
   This documentation is a work in progress and may be restructured at any time.

This site serves to document the Lua API of the very excellent game, Psychonauts, created by Double Fine.

The game uses Lua for a large portion of its code, including for practically every entity in the game (Raz, Censors, etc.) as well as level scripting. As such the API is incredibly extensive.

As the engine itself is proprietary and in-house by Double Fine, some parts of it may be unclear or opaque for modders, hence why this documentation exists.

.. toctree::
   :maxdepth: 1
   :caption: Contents

   self
   lua/index
   cppapi/index
   tutorials/index