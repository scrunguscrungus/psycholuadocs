C++ API
=======

The C++ API used to interface with the game engine in various ways.

All functions of the C++ API are global.

.. toctree::
   :maxdepth: 2
   :hidden:

LSO
""""""""""""""""""
.. lua:class:: LSO

	Not technically part of the API but detailed here for a lack of better place to put it.
	
	LSOs are the engine's method of interfacing with Lua scripted entities. Every script class is linked to an LSO.

	If a function takes/returns or LSO, this can be thought of as that function just taking/returning a script object.

CreateObject
""""""""""""""""""
.. lua:method:: CreateObject(BaseClass, ClassName)

	Creates and returns an :lua:class:`LSO`. For use in entity constructors. Not to be confused with :lua:meth:`SpawnScript`

	:param BaseClass: Specifies the class to inherit from. All classes should ultimately have ScriptBase at the top of their inheritance chain. Case-sensitive.
	:type BaseClass: string
	:param ClassName: This should match the name of the class exactly. This parameter is used to do internal setup for when another class wishes to inherit from this one.
	:type ClassName: string

	:return: The resulting LSO
	:rtype: |LSO|

SpawnScript
""""""""""""""""""
.. lua:method:: SpawnScript(ClassName, Name, EditVars, has-entity, bDeleteOnLevelUnload)

	Spawns a script class.

	:param ClassName: Specifies the class to create
	:type ClassName: string

	:param Name: Specifies what the created entity should be called
	:type Name: string

	:param EditVars: Specifies the editvars of the entity
	:type EditVars: string

	:param has-entity: Specifies if a corresponding entity is created. It's currently unclear what this entails. Causes an error if used with bDeleteOnLevelUnload=0.
	:type has-entity: :lua:class:`bool`

	:param bDeleteOnLevelUnload: Controls whether the created entity will be removed when the level unloads. Causes an error if used with bDeleteOnLevelUnload=1.
	:type bDeleteOnLevelUnload: :lua:class:`bool`

GamePrint
""""""""""""""""""
.. lua:method:: GamePrint(message)

	Prints a message to the debug console

	:param message: The message to print
	:type message: string
	