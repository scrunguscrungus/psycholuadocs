Tutorials
=========

.. toctree::
   :maxdepth: 2
   :hidden:

Here you can find help and guides on how to accomplish different tasks when writing scripts for Psychonauts.

.. toctree::
   :maxdepth: 1
   :caption: Tutorials
   
   classnaming
   newclass