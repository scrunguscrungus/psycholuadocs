Classnames
==========

.. toctree::
   :maxdepth: 2
   :hidden:

Classes are assigned a canonical name based on their path. Canonical classnames essentially just replace the slashes in the path following ``Scripts/`` with periods and remove the ``.lua`` extension.

For example, the classname of a script located at ``Scripts/MM/Characters/Boyd.lua`` would become ``mm.characters.Boyd``

.. important:: 
	The name of the class itself (i.e "Boyd" in the example above) is case-sensitive. Note, however, that everything preceding it is not case-sensitive, though traditionally presented as lowercase.

Entities
^^^^^^^^
Class names for entities in the base game (except certain core classes and those created early in development) follow a specific structure:
``[level code].[type].[class name]``

This scheme is (mostly) just for the sake of organisation though it's encouraged to follow this standard. Level script classes are an exception (described below).

``Level code`` is the two letter level code of the level this class is for. For example, ``MMI1`, `MMI2`` and ``MMDM`` all share the level code ``MM``. Entities that appear in many levels usually use ``Global`` for this, or are placed in the root directory (which is discouraged).

``Type`` is the type of entity. A list of types used by most levels can be found below.

``Class name`` is, finally, just the name of your class.

Most levels in the base game use the following types:

Characters
	Used for, well, characters. This is fairly self-explanatory, though the game sometimes categorises things such as projectiles and other such objects under this type.
Cutscenes
	EZScene cutscenes.
Effects
	Effect classes. Explosions, lasers, you name it. Particle effects generally live here.
Props
	Used for inanimate objects of all shapes and sizes.

Types are generally not strict and it is not necessary to stick to this scheme for your own scripts though it is encouraged. You are also free to use your own categorisation. The base game's ``Global`` domain includes many types used nowhere else.

Levels
^^^^^^
.. warning:: 
	Not having a level script class can lead to **catastrophic failure** so you should always be sure to have one for your level.

	This may not always be clear if developing with full unpacked Lua sources as the game will load global.DefaultLevelScript if no script exists for a level, but this file **does not** ship with the final game and most players won't have it!

Level scripts are unique in that when loading a level the game is hardcoded to expect to find the level script at ``[level code].[level name]``. For example, if your level is called ``MMI1`` then the game will try to spawn ``mm.MMI1`` as the level script.

Right now there is no way around this hardcoded search location though Astralathe may provide one at some point.