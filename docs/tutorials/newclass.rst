Creating a new class
====================

.. toctree::
   :maxdepth: 2
   :hidden:

.. note:: This document is a work in progress and incomplete.

Creating a new class is fairly simple.

To start, create a Lua script at the correct location depending on what you wish your classname to be. See :ref:`Classnames` for information on how the game handles classnames internally and how you should name and organise your classes.

For this example, we're creating a class simply named ``MyClass``. This will be a simple class that has a configurable mesh and prints an incrementing number to the debug console when interacted with by Raz.

Class Constructors
""""""""""""""""""
Class constructors are simply functions. The name of the function must exactly match the name of the entity's file and is case-sensitive.

Constructors take one argument, traditionally called ``Ob`` and must return this back when done. At the start of the function the constructor should check if ``Ob`` is nil, and if it is not should use :lua:meth:`CreateObject` to create it.

.. code-block:: lua
	:caption: A simple entity constructor
	:linenos:

	function MyClass(Ob)
		if ( not Ob ) then
			Ob = CreateObject("ScriptBase"); --Inherit directly from ScriptBase
		end

		return Ob; --ALWAYS be sure to return Ob!
	end
	
Above is an example of the most simple class constructor possible. This class is spawnable ingame but will do nothing and won't appear as anything in the world.